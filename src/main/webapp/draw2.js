var graphviz;
var i0 = 3;
var n = 2;
var i = i0;
function drawButton() {
    document.getElementById("graph").innerHTML = "";
    sendRequest("/api?c=draw&n="+i, function (http) {
        graphviz = d3.select("#graph").graphviz()
            .transition(function () {
                return d3.transition("main")
                    .ease(d3.easeLinear)
                    .delay(500)
                    .duration(1500);
            });
        var result = http.responseText;
        if (result != "") {
            let json = JSON.parse(result)
            graphviz.renderDot(json["result"]);
        }
    });
}

function drawNextButton() {
    i = (i + 1 - i0) % n + i0;
    sendRequest("/api?c=draw&n="+i, function (http) {
        var result = http.responseText;
        if (result != "") {
            let json = JSON.parse(result)
            graphviz.renderDot(json["result"]);
        }
    });
}
