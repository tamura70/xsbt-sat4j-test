function sat4j() {
    var outputNode = document.getElementById("output");
    outputNode.innerHTML = "Solving";
    var cnf = document.getElementById("cnf").value;
    var data = { "c" : "sat4j", "cnf" : cnf };
    postRequest("/api", data, function (http) {
        var result = http.responseText;
        if (result != "") {
            outputNode.innerHTML = "Done";
            var json = JSON.parse(result)
            outputNode.innerHTML = "<pre>\n" + escapeHTML(json["result"]) + "</pre>\n";
        }
    });
}
