function drawButton(n) {
    sendRequest("/api?c=draw&n="+n, function (http) {
        var result = http.responseText;
        if (result != "") {
            var json = JSON.parse(result)
            document.getElementById("graph").innerHTML = "";
            d3.select("#graph").graphviz()
                .fade(false)
                .renderDot(json["result"]);
        }
    });
}
