package web

class Digraph {
  var index: Int = 0
  var nodeMap: Map[String,Int] = Map.empty
  var nodeOpt: Map[Int,String] = Map.empty
  var arcs: Seq[(Int,Int)] = Seq.empty
  var arcOpt: Map[(Int,Int),String] = Map.empty

  def node(node: String): Int = {
    if (nodeMap.contains(node)) {
      nodeMap(node)
    } else {
      index += 1
      nodeMap += node -> index
      index
    }
  }
  def setOpt(name: String, opt: String): Unit = {
    val i = node(name)
    nodeOpt += i -> opt
  }
  def setLabel(node: String, label: String): Unit =
    setOpt(node, s"""label="$label"""")
  def addArcOpt(node1: String, node2: String, opt: String = ""): Unit = {
    val arc = (node(node1),node(node2))
    arcs :+= arc
    if (opt != "")
      arcOpt += arc -> opt
  }
  def addArc(node1: String, node2: String, label: String = ""): Unit =
    if (label == "") addArcOpt(node1, node2)
    else addArcOpt(node1, node2, s"""label="$label"""")
  def toGraphviz: String = {
    val fontname = "helvetica";
    val sb = new StringBuilder
    sb.append("digraph { ");
    sb.append("rankdir=LR; ");
    sb.append(s"""graph [fontname="$fontname"]; """)
    sb.append(s"""node [fontname="$fontname"]; """)
    sb.append(s"""edge [fontname="$fontname"]; """)
    for (node <- nodeMap.keys) {
      val i = nodeMap(node)
      if (nodeOpt.contains(i)) {
        val opt = nodeOpt(i)
        sb.append(s"""n$i [$opt]; """)
      } else {
        sb.append(s"""n$i [label="$node"]; """)
      }
    }
    for (j <- 0 until arcs.size) {
      val arc = arcs(j)
      val (i1,i2) = arc
      if (arcOpt.contains(arc)) {
        val opt = arcOpt(arc)
        sb.append(s"""n$i1 -> n$i2 [$opt]; """)
      } else {
        sb.append(s"n$i1 -> n$i2; ")
      }
    }
    sb.append("}");
    sb.toString
  }
}
