package web

import org.sat4j.specs.ISolver
import org.sat4j.specs.ISolverService
import org.sat4j.specs.IConstr
import org.sat4j.specs.Constr
import org.sat4j.specs.Lbool
import org.sat4j.specs.SearchListener
import org.sat4j.specs.RandomAccessModel
import org.sat4j.specs.TimeoutException
import org.sat4j.specs.ContradictionException

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

class MiniSat[D <: org.sat4j.minisat.core.DataStructureFactory](
  learning: org.sat4j.minisat.core.LearningStrategy[D],
  dsf: D,
  order: org.sat4j.minisat.core.IOrder,
  restart: org.sat4j.minisat.core.RestartStrategy
) extends org.sat4j.minisat.core.Solver[D](learning, dsf, order, restart) {
  // Set of original constraints
  def getConstrs: org.sat4j.specs.IVec[Constr] = constrs
  // def getIthConstr(i: Int)
  // def getLearnedConstraints
  // variable assignments (literals) in chronological order
  def getTrail: org.sat4j.specs.IVecInt = trail
  // position of the decision levels on the trail
  def getTrailLim: org.sat4j.specs.IVecInt = trailLim
  // position of assumptions before starting the search
  def getRootLevel: Int = rootLevel
  // getLevel
  def getLevel(p: Int): Int = getVocabulary.getLevel(p)
  // getReason
  def getReason(p: Int): Constr = getVocabulary.getReason(p)
}

object MiniSat {
  import org.sat4j.minisat.constraints.{MixedDataStructureDanielWL => D}
  def apply() = {
    val dsf = new D()
    val learning = new org.sat4j.minisat.learning.MiniSATLearning[D]()
    val order = new org.sat4j.minisat.orders.VarOrderHeap()
    val restart = new org.sat4j.minisat.restarts.MiniSATRestarts()
    new MiniSat(learning, dsf, order, restart)
  }
}

class Sat4j(dimacsString: String) extends SearchListener[ISolverService] {
  val sat4jSolver: ISolver = MiniSat()
  val timeout = 10000

  // toDimacs
  def toDimacs(p: Int): Int = org.sat4j.core.LiteralsUtils.toDimacs(p)

  def log(s: String): Unit = {
    print("# ")
    println(s)
  }

  /*
   * Listeners
   */
  // Provide access to the solver's controllable interface
  def init(solverService: ISolverService): Unit = {
    log("init")
  }
  // decision variable
  def assuming(lit: Int): Unit = {
    log(s"assuming: $lit")
  }
  // Unit propagation
  def propagating(lit: Int, reason: IConstr): Unit = {
    log(s"propagating: $lit, $reason")
  }
  // backtrack on a decision variable
  def backtracking(lit: Int): Unit = {
    log(s"backtracking: $lit")
  }
  // adding forced variable (conflict driven assignment)
  def adding(lit: Int): Unit = {
    log(s"adding: $lit")
  }
  // learning a new clause
  def learn(clause: IConstr): Unit = {
    log(s"learn: $clause")
    sat4jSolver match {
      case minisat: MiniSat[_] => {
        val learned = minisat.getLearnedConstraints
        for (i <- 0 until learned.size) {
          val c = learned.get(i)
          log(s"  learned($i) = $c")
        }
      }
    }
  }
  // learn a new unit clause (a literal)
  def learnUnit(lit: Int): Unit = {
    log(s"learnUnit: $lit")
  }
  // delete a clause
  def delete(clause: IConstr): Unit = {
    log(s"delete: $clause")
  }
  // a conflict has been found
  def conflictFound(confl: IConstr, dlevel: Int, trailLevel: Int): Unit = {
    log(s"conflictFound: $confl, $dlevel, $trailLevel")
    sat4jSolver match {
      case minisat: MiniSat[_] => {
        val trail = minisat.getTrail
        for (i <- 0 until trail.size) {
          val p: Int = trail.get(i)
          val reason: Constr = minisat.getReason(p)
          val x: Int = toDimacs(p)
          log(s"  trail($i) = $x, reason = $reason")
        }
        val trailLim = minisat.getTrailLim
        for (i <- 0 until trailLim.size) {
          log(s"  trailLim($i) = " + trailLim.get(i))
        }
      }
    }
  }
  // a conflict has been found while propagating values
  def conflictFound(lit: Int): Unit = {
    log(s"conflictFound: $lit") // ???
  }
  // a solution is found
  def solutionFound(model: Array[Int], lazyModel: RandomAccessModel): Unit = {
    log("solutionFound")
  }
  // starts a propagation
  def beginLoop(): Unit = {
    log("beginLoop")
  }
  // Start the search
  def start(): Unit = {
    log("start")
  }
  // End the search
  def end(result: Lbool): Unit = {
    log(s"end: $result")
  }
  // The solver restarts the search
  def restarting(): Unit = {
    log("restarting")
  }
  // The solver is asked to backjump to a given decision level
  def backjump(backjumpLevel: Int): Unit = {
    log(s"backjump: $backjumpLevel")
  }
  // The solver is going to delete some learned clauses
  def cleaning(): Unit = {
    log("cleaning")
  }

  def solve(): String = {
    sat4jSolver.setSearchListener(this);
    val sb = new StringBuilder
    try {
      val reader = new org.sat4j.reader.DimacsReader(sat4jSolver)
      val str = dimacsString.replaceAll("""\s*\r?\n\s*""", "\n")
      val in = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))
      val problem = reader.parseInstance(in)
      sat4jSolver.setTimeoutMs(timeout)
      if (problem.isSatisfiable()) {
        sb.append("SAT\n")
        val model = problem.model()
        for (i <- 0 until model.length) {
          sb.append(model(i))
          sb.append(" ")
        }
        sb.append("0\n")
      } else {
        sb.append("UNSAT\n")
      }
    } catch {
      case e: ContradictionException => {
        sb.append("UNSAT\n")
      }
      case e: TimeoutException => {
        sb.append("TIMEOUT\n")
      }
      case e: Exception => {
        sb.append(s"ERROR ${e.getMessage}\n")
        println(e)
      }
    } finally {
      sat4jSolver.setTimeoutMs(0)
      sat4jSolver.reset
    }
    sb.toString
  }
}
