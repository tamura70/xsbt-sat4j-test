package web

class Digraph2 {
  var nodeIndex: Int = 0
  var nodeMap: Map[String,Int] = Map.empty
  var nodeOpt: Map[Int,Map[String,String]] = Map.empty
  var arcs: Seq[(Int,Int)] = Seq.empty
  var arcOpt: Map[(Int,Int),Map[String,String]] = Map.empty
  var levelNodesMap: Map[Int,Set[Int]] = Map.empty

  def addNode(node: String): Int = {
    if (nodeMap.contains(node)) {
      nodeMap(node)
    } else {
      nodeIndex += 1
      nodeMap += node -> nodeIndex
      nodeOpt += nodeIndex -> Map("label" -> node)
      nodeIndex
    }
  }
  def addNodeOpt(node: String, opts: Map[String,String]): Unit = {
    val i = addNode(node)
    nodeOpt += i -> (nodeOpt(i) ++ opts)
  }
  def addArc(node1: String, node2: String, opts: Map[String,String] = Map.empty): Unit = {
    val arc = (addNode(node1),addNode(node2))
    arcs :+= arc
    if (opts.size != 0)
      arcOpt += arc -> opts
  }
  def setLevel(node: String, level: Int): Unit = {
    val i = addNode(node)
    val nodes = levelNodesMap.getOrElse(level, Set.empty)
    levelNodesMap += level -> (nodes + i)
  }
  def toGraphviz: String = {
    val fontname = "helvetica";
    val sb = new StringBuilder

    def nodeDefinition(i: Int): String = {
      val opt = nodeOpt(i).map {
        case (k,v) => s"""$k = "$v""""
      }.mkString(",")
      s"""  n$i [$opt];\n"""      
    }
    sb.append("digraph {\n");
    sb.append("rankdir=LR;\n");
    sb.append(s"""graph [fontname="$fontname"];"""+"\n")
    sb.append(s"""node [fontname="$fontname"];"""+"\n")
    sb.append(s"""edge [fontname="$fontname"];"""+"\n")
    var nodes = (1 to nodeIndex).toSet
    for (level <- levelNodesMap.keys.toSeq.sorted) {
      sb.append(s"""subgraph cluster_$level {"""+"\n")
      sb.append(s"""  label = "Level $level";"""+"\n")
      sb.append(" labelloc = t;\n")
      sb.append(" labeljust = l;\n")
      for (i <- levelNodesMap(level))
        sb.append(nodeDefinition(i))
      sb.append("}\n")
      nodes = nodes diff levelNodesMap(level)
    }
    for (i <- nodes)
      sb.append(nodeDefinition(i))
    for (a <- 0 until arcs.size) {
      val arc = arcs(a)
      val (i1,i2) = arc
      if (arcOpt.contains(arc)) {
        val opt = arcOpt(arc).map {
          case (k,v) => s"""$k = "$v""""
        }.mkString(",")
        sb.append(s"""n$i1 -> n$i2 [$opt];"""+"\n")
      } else {
        sb.append(s"""n$i1 -> n$i2;"""+"\n")
      }
    }
    sb.append("}\n");
    println(sb.toString)
    sb.toString
  }
}
