package web

/*
   https://github.com/json4s/json4s
 */

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._

object Command {
  /* https://stackoverflow.com/questions/23348480/json4s-convert-type-to-jvalue */
  def encodeJson(src: AnyRef): JValue = {
    import org.json4s.{ Extraction, NoTypeHints }
    import org.json4s.JsonDSL.WithDouble._
    import org.json4s.jackson.Serialization
    implicit val formats = Serialization.formats(NoTypeHints)
    Extraction.decompose(src)
  }
 
  def handler(command: String, params: Map[String,Seq[String]]): JValue = command match {
    case "date" => {
      val fmt = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
      val str = fmt.format(new java.util.Date)
      JString(str)
    }
    case "draw" => {
      params.get("n") match {
        case Some(Seq("1")) => {
          JString("digraph { a -> b; }")
        }
        case Some(Seq("2")) => {
          val dg = new Digraph
          dg.addArc("a", "b")
          dg.addArc("a", "c", "x")
          dg.addArc("b", "d")
          dg.addArcOpt("c", "d", "dir=both,style=dashed")
          dg.setOpt("a", """label="a",tooltip="XXX"""")
          dg.setOpt("d", "label=sink,shape=doublecircle")
          JString(dg.toGraphviz)
        }
        case Some(Seq("3")) => {
          val dg = new Digraph2
          // conflictFound: 2[F] 3[F] 1[F] , 2, 3
          dg.addNodeOpt("Conflict", Map("tooltip" -> "2 3 1"))
          dg.setLevel("Conflict", 2)
          dg.addArc("-2", "Conflict")
          dg.addArc("-3", "Conflict")
          dg.addArc("-1", "Conflict")
          // trail(0) = -1, reason = null
          dg.addNodeOpt("-1", Map("tooltip" -> "null"))
          dg.setLevel("-1", 1)
          // trail(1) = -3, reason = null
          dg.addNodeOpt("-3", Map("tooltip" -> "null"))
          dg.setLevel("-3", 2)
          // trail(2) = -2, reason = -2[T] 3[F] 1[F]
          dg.addNodeOpt("-2", Map("tooltip" -> "-2 3 1"))
          dg.setLevel("-2", 2)
          dg.addArc("-3", "-2")
          dg.addArc("-1", "-2")
          //
          JString(dg.toGraphviz)
        }
        case Some(Seq("4")) => {
          val dg = new Digraph2
          // conflictFound: 2[F] -3[F] 1[F] , 1, 3
          dg.addNodeOpt("Conflict", Map("tooltip" -> "2 -3 1"))
          dg.setLevel("Conflict", 1)
          dg.addArc("-2", "Conflict")
          dg.addArc("3", "Conflict")
          dg.addArc("-1", "Conflict")
          // trail(0) = -1, reason = null
          dg.addNodeOpt("-1", Map("tooltip" -> "null"))
          dg.setLevel("-1", 1)
          // trail(1) = 3, reason = 3[T] 1[F]
          dg.addNodeOpt("3", Map("tooltip" -> "3 1"))
          dg.setLevel("3", 1)
          dg.addArc("-1", "3")
          // trail(2) = -2, reason = -2[T] -3[F] 1[F] 
          dg.addNodeOpt("-2", Map("tooltip" -> "-2 -3 1"))
          dg.setLevel("-2", 1)
          dg.addArc("3", "-2")
          dg.addArc("-1", "-2")
          //
          JString(dg.toGraphviz)
        }
        case _ => JNull
      }
    }
    case "sat4j" => {
      params.get("cnf") match {
        case Some(Seq(dimacsString)) => {
          val sat4j = new Sat4j(dimacsString)
          val output = sat4j.solve()
          JString(output)
        }
        case _ => {
          JNull
        }
      }
    }
    case _ => {
      JNull
    }
  }

  def exec(command: String, params: Map[String,Seq[String]]): Option[String] = {
    println(s"Command.exec: $command, $params")
    val result: JValue = handler(command, params)
    if (result == JNull) {
      None
    } else {
      val json = 
        ("command" -> command) ~
        ("params" -> params) ~
        ("result" -> result)
      Some(compact(render(json)))
    }
  }
}

